For potato datasets:

1. Potato-leaf-disease-classification-using-CNN: https://github.com/Abpatel1301/Potato-leaf-disease-classification-using-CNN

2. potato_leaf_disease project: https://www.kaggle.com/code/shivamvatshayan/potato-leaf-disease-project

*3. Leaf Disease Prediction using CNN: https://www.kaggle.com/code/hamedetezadi/leaf-disease-prediction-using-cnn


*4. potato_leaf_disease 97-100% accuracy: https://www.kaggle.com/code/priyark/potato-leaf-disease-97-100-accuracy




Refer similar cnn models:

5. Potato Leaf Disease Classifier Demo: https://www.kaggle.com/code/sakthimurugavel/potato-leaf-disease-classifier-demo


6. Image Classification using Convolutional Neural Networks in Keras: https://learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras/

7. Image Classification Using CNN -Understanding Computer Vision: https://www.analyticsvidhya.com/blog/2021/08/image-classification-using-cnn-understanding-computer-vision/

8. Binary Image classifier CNN using TensorFlow: https://medium.com/techiepedia/binary-image-classifier-cnn-using-tensorflow-a3f5d6746697

9. CNN Model With PyTorch For Image Classification: https://medium.com/thecyphy/train-cnn-model-with-pytorch-21dafb918f48